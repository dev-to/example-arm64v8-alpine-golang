package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"runtime"
	"strings"
)

func main() {
	fmt.Println("Hello World")
	fmt.Println()

	fmt.Println("### runtime.GOOS && runtime.GOARCH ###########")
	fmt.Println(runtime.GOOS)
	fmt.Println(runtime.GOARCH)
	fmt.Println()

	fmt.Println("### os.Setenv() && os.Getenv() ###############")
	os.Setenv("FOO", "1")
	fmt.Println("FOO:", os.Getenv("FOO"))
	fmt.Println("BAR:", os.Getenv("BAR"))
	fmt.Println()

	fmt.Println("### os.Environ() #############################")
	for _, e := range os.Environ() {
		pair := strings.Split(e, "=")
		fmt.Println(pair[0])
	}
	fmt.Println()

	fmt.Println("### ioutil.ReadDir(\"./\") #####################")
	files, err := ioutil.ReadDir("./")
	if err != nil {
		log.Fatal(err)
	}

	for _, f := range files {
		fmt.Println(f.Name())
	}
	fmt.Println()

	fmt.Println("### os.Stat(\"/work\"); !os.IsNotExist(err) ####")
	if _, err := os.Stat("/work"); !os.IsNotExist(err) {
		workFiles, err := ioutil.ReadDir("/work")
		if err != nil {
			log.Fatal(err)
		}

		for _, f := range workFiles {
			fmt.Println(f.Name())
		}
	} else {
		fmt.Println("\"/work\" is not exist.")
	}
	fmt.Println()
}
