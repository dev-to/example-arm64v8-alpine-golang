FROM golang:1.13.5-alpine3.10 AS GO-BUILD-ENV
COPY main.go .
RUN CGO_ENABLED=0 GOOS=linux go build  -ldflags '-w -s' -a -installsuffix cgo -o dist/main && \
    cp dist/main /main && \
    chmod +x /main && \
    /main && \
    uname -a && \
    uname -m && \
    go version

FROM scratch
LABEL maintainer="Anucha Nualsi <ana.cpe9@gmail.com>"
COPY --from=GO-BUILD-ENV /main /
CMD ["/main"]
